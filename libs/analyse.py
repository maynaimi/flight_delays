import pandas as pd
import numpy as np

def find_delay_summary_by_airline(df):
    # Find total flights by airline
    dfTotal = pd.DataFrame({'NUM_FLIGHTS' : df.groupby('CARRIER').size()})

    # Find total late arrivals by airline
    dfTotalDelayed = pd.DataFrame({'NUM_DELAYED' : \
        df.groupby('CARRIER')['ARR_DELAY_NEW'].apply(lambda x: x[x > 0].count())})

    # Find total cancelled flights
    dfTotalCancelled = pd.DataFrame({'NUM_CANCELLED' : \
        df.groupby('CARRIER')['CANCELLED'].apply(lambda x: x[x > 0].count())})

    dfSummary = pd.concat([dfTotal, dfTotalDelayed, dfTotalCancelled], axis=1)

    # Find percent of flights with late arrival by airline
    dfSummary['PERC_DELAY_FLIGHTS'] = (100 * dfSummary['NUM_DELAYED'] / dfSummary['NUM_FLIGHTS']).round(2)

    # Find percent of flights which are cancelled
    dfSummary['PERC_CANCELLED_FLIGHTS'] = (100 * dfSummary['NUM_CANCELLED'] / dfSummary['NUM_FLIGHTS']).round(2)

    # Find mean amount late arrival by airline
    dfSummary['MEAN_DELAY'] = df.groupby('CARRIER')['ARR_DELAY_NEW'].mean().round(2)
   
    return dfSummary
    

def find_least_trustworthy_airline(df, delayedW=0.5, cancelledW=0.3, meanDelayW=0.2):
    dfNorm = (df - df.mean()) / (df.max() - df.min())
    dfWeighted = \
        dfNorm['PERC_DELAY_FLIGHTS'] * delayedW + \
        dfNorm['PERC_CANCELLED_FLIGHTS'] * cancelledW + \
        dfNorm['MEAN_DELAY'] * meanDelayW

    return dfWeighted.idxmax() 

def find_perc_arrival_delay(df):
    numDelayedDep = df.shape[0]
    numDelayedArr = df[df['ARR_DELAY_NEW'] > 0].shape[0]
    return 100 * numDelayedArr / numDelayedDep

def find_dep_arr_difference(df):
    return (df['ARR_DELAY_NEW'] - df['DEP_DELAY_NEW']).mean()

def get_delayed_dep_flights(df):
    return df[df['DEP_DELAY_NEW'] > 0]