import pandas as pd

fields = ['FL_DATE'
        , 'CARRIER'
        , 'FL_NUM'
        , 'DEP_DELAY_NEW'
        , 'ARR_DELAY_NEW'
        , 'CANCELLED'
]
# Might need these: 
# CANCELLED,CANCELLATION_CODE,CARRIER_DELAY,WEATHER_DELAY,
# NAS_DELAY,SECURITY_DELAY,LATE_AIRCRAFT_DELAY,
# AIRLINE_ID, UNIQUE_CARRIER

def parseFile(fileName):
    return pd.read_csv(fileName, usecols=fields)