# Flight Delays
Solution to Stocard's Flight case (Part 1) - Flight delays

## Questions
1. Which airline would you trust least to reach your destination in time?
2. What is the relationship between the departure delay and arrival delay, if you consider only flights that have a departure delay when they take off?

## Run Program
To run the program to calculate the answers to the provided questions, you need python 3.6+.

You'll also need the pandas module:
```
pip install pandas
```

Then to run, execute:
```
python analyse_data.py
```
Some details will be provided in the output

## Answers
As a summary, the answers found were:
1. I would trust VX least to get to my destination on time.
2. Overall, 64% of flights that are delayed when departing are also delayed when arriving.

## Notes
### General
* We only have 1 weeks' worth of data, so drawing conclusions from it might be a little misleading.

### Question 1
* Looks like (at least with the sample provided) that the AIRLINE_ID == UNIQUE_CARRIER == CARRIER. So we are only using the CARRIER for our analysis.
* As an improvement, we could assume that the only 'delay' type that is in the hands of the airline is the 'CARRIER_DELAY', so in cases where it is set, it could be used instead of the 'ARR_DELAY_NEW' (or the other delays could be subtracted)
* We include whether the flight has been cancelled in our least trustworthy calculation, but from the provided data, we cannot tell why the flights are cancelled. To compensate, we've given this feature a lower weighted value.

### Question 2
* None, other than what's in the comments!

## TODOs
* We should really be cleaning the data before doing any analysis on it
* We should be handling errors
* Some testing, especially for the calculations
* Some more verbose logging might be helpful in some cases
* Some visualisations would be nice

## Built With
* [Python](https://www.python.org/) - Programming language (version 3.6.5)
* [Panda](https://pandas.pydata.org/) - Python data analysis library

## Authors
* May Naimi-Jones