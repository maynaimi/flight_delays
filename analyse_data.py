from libs import parse
from libs import analyse

# TODO: Error handling, data cleansing, visualisations

if __name__ == "__main__":
    # Retrieve relevant data in a DataFrame
    df = parse.parseFile('data/dataset_delays.csv')

    ### Question 1 - Which airline would you trust least to reach your destination in time?
    print("\n*** Question 1 ***\n")

    # We collect some details to help us answer the question and then decide
    dfSummary = analyse.find_delay_summary_by_airline(df)
    print("Summary of airline delays:\n")
    print(dfSummary)

    leastTrustworthyAirline = analyse.find_least_trustworthy_airline(df=dfSummary, delayedW=0.5, cancelledW=0.3, meanDelayW=0.2);
    print("\nI would trust %s least to get to my destination on time." % (leastTrustworthyAirline))


    ### Question 2 - What is the relationship between the departure delay and arrival delay, if you 
    # consider only flights that have a departure delay when they take off?
    print("\n*** Question 2 ***\n")  
    
    # For this question we only need to look at the delayed flights
    dfDelayedDep = analyse.get_delayed_dep_flights(df)

    # We calculate the percentage of delayed flights which also arrive late
    percLateArrival = analyse.find_perc_arrival_delay(dfDelayedDep)
    print("Overall, %d%% of flights that are delayed when departing are also delayed when arriving." 
        % percLateArrival)

    # This also tells us something, but it wouldn't be a factor specific to only delayed flights
    deptArrDelayDifference = analyse.find_dep_arr_difference(dfDelayedDep)
    textShorterLonger = 'longer' if deptArrDelayDifference > 0 else 'shorter'
    print("On average, flights with a departure delay have a %0.1fmin %s arrival delay.\n" 
        % (abs(deptArrDelayDifference), textShorterLonger))
   